import TSP_biblio
def mat_dist(fichier):
    '''
    :lecture du fichier csv
    :renvoie la matrice des distances entre chacune des villes
    :param fichier:file
    :return list_dist: list
    '''
    tour=TSP_biblio.get_tour_fichier(fichier)
    list_dist=[]
    l=len(tour)
    for i in range(l):
        ligne=[]
        for j in range(l):
            ligne.append(TSP_biblio.distance(tour,i,j))
        list_dist.append(ligne)
    return list_dist

def ville_plus_proche(ville,liste_ville,mat_dist):
    '''
    :renvoie la ville la plus proche d'une liste de villes
    :param ville:string
    :param: liste_ville:list
    :param mat_dist:list
    :return:string
    '''
    dist=[]
    for i in liste_ville:
        dist.append(mat_dist[ville][i])
    min_dist=min(dist)
    return(mat_dist[ville].index(min_dist))
   
    
def glouton(ind_ville,liste_ville,mat_dist):
    '''
    :retourne la liste des indices des villes des plus proches
    :param ind_ville:int indice de la ville
    :param liste_ville:list
    :param mat_dist:list
    :return:list
    '''
    l=len(liste_ville)
    tour=[ind_ville]
    liste_ville.remove(ind_ville)
    while len(tour)!=l:
        ind_ville=ville_plus_proche(ind_ville,liste_ville,mat_dist)
        tour.append(ind_ville)    
        liste_ville.remove(ind_ville)
    return(tour)

def liste_ville(fichier):
    '''
    :retourne la liste des indices des villes de 0 à la longueur
    :param fichier:file
    :return:list
    
    '''
    tour=TSP_biblio.get_tour_fichier(fichier)
    l=len(tour)
    liste_ville=[k for k in range(l)]
    return liste_ville



def indice_ville(ville,fichier):
    '''
    :retourne l'indice de la ville dans le fichier
    :param ville:string
    :param fichier:file
    :return:int
    
    '''
    tour=TSP_biblio.get_tour_fichier(fichier)
    liste_ville_vers_indice=[]
    for i in range(len(tour)):
        liste_ville_vers_indice.append(tour[i][0])
    return(liste_ville_vers_indice.index(ville))
    indice=tour.index(ville)
    
def tournee(ville,fichier):
    '''
    :creation de la tournee the tour et affichage
    '''
    ind_ville=indice_ville(ville,fichier)#indice de la ville entree
    mat_distance=mat_dist(fichier)#matrice des distances
    liste_des_villes=liste_ville(fichier)#liste des indices villes
    tour=TSP_biblio.get_tour_fichier(fichier)
    tournee_liste_indice=glouton(ind_ville,liste_des_villes,mat_distance)
    the_tour=[]
    for i in tournee_liste_indice:
        the_tour.append(tour[i])
    print(the_tour)
    print(TSP_biblio.longueur_tour(the_tour))
    TSP_biblio.trace(the_tour)
tournee('"Boulogne"',"exemple.txt")
    
    
    
    


          
    
    
        
    

    
    
    
    
    
            

        
    
    
    
